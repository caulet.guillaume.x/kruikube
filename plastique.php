<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="Asset/Image/cubefav.png" type="image/x-icon">
    <meta name="description" content="">
    <link rel="stylesheet" href="style.css">
    <title>KruiKube - Plastique Bleu</title>
</head>

<body>

    <?php include 'header.php' ?>

    <div class="content-container">
        <img class="image-cube" src="./Asset/Image/plastique blue.png" alt="KruiKube Plastique Bleu">
        <div class="content">
            <h1 class="title">KruiKube Plastique <span class="bleu">Bleu</span></h1>
            <p>Matériau : PVC</p>
            <p>Prix : 29.99€</p>
            <p>Dimensions : 16 x 16 cm</p>
            <p>Description :</p>
            <p>Voir en avez toujours rêvé, nous l'avons fait. Pour votre plus grand plaisir, le Kube bleu fait son apparition.</p>
        </div>
    </div>

    <div class="page-description">
        <h2 class="title">Plastique Bio Recyclé</h2>
        <p>Chez Kruikube nous nous assurons personnellement de la qualité de notre plastique.
            Chez nous, notre plastique est plastifié et replastifié avant d'être livré
            dans son emballage plastique. Contrairement à nos concurrents qui
            en sont réduit à vouloir interdire nos emballages en prétendant la sauvegarde
            de notre planête, alors que nous savons et vous savez également très bien que
            ce sont des capitalistes sans le moindre égard ni pour la planète, ni pour le vert
            verdoyant de la verdure de notre garrigue.
        </p>

        <p>
            Ainsi parmis nos fournisseurs en plastique on retrouve l'entreprise locale Place-Toques,
            situé sur la place Toques dirigé par l'ancien restaurateur Philippe Isthebest dont la
            toque à provoqué de nombreux cauchemards en cuisine.
        </p>
        <p>
            Mais nos ne nous reposons pas seulement sur la production locale, surtout pendant
            les periodes de chasse ou notre village d'irréductible gaulois rapporte de nombreux sangliers
            forçeant le chef à reprendre du service derrière les fourneaux.
        </p>
        <p>
            C'est donc pour cela qu'en ces periodes de creux, pour ne pas dire de petit creux,
            que la delicieuse, gouteuse, et raffiné multinationale Cuisse-Tôt nous
            cède son surplus de plastique.
            Surplus dont la qualité n'est pas à remettre en cause, puisque nous
            nous occupons personnellement d'en verifier la qualité.
            Cette tache revient donc à nos plus fidèles employés Tique et Taque, dont l'embauche
            la semaine dernière nous a excessivement ravi puisque nous les avons debauché chez l'un
            de nos partenaire, qui n'est autre que la grande, magnifique, merveilleuse et
            majestueuse multinational Arts-Naqueur elle même.

            Ainsi donc nous avons pu eviter tout eventuel, pour ne pas dire probable,
            conflit d'interêt non pas bancaire mais qualitatif (en terme de bois), et ce
            dans le but de pouvoir assurer à notre genereuse clientelle une qualité de conception
            des plus certaines.
        </p>
    </div>

    <script type="application/ld+json">
        [{
                "@context": "https://schema.org/",
                "@type": "Product",
                "name": "KruiKube Plastique",
                "image": [
                    "./Asset/plastique blue.png"

                ],
                "description": "",
                "sku": "KruiKube01",
                "mpn": "925872",
                "brand": {
                    "@type": "Brand",
                    "name": "KruiKube"
                },
                "review": {
                    "@type": "Review",
                    "reviewRating": {
                        "@type": "Rating",
                        "ratingValue": "4.7",
                        "bestRating": "5"
                    },
                    "author": {
                        "@type": "Person",
                        "name": "Jean KruiKube"
                    }
                },
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue": "4.92",
                    "reviewCount": "3956"
                },
                "offers": {
                    "@type": "Offer",
                    "url": "urldelapage",
                    "priceCurrency": "EUR",
                    "price": "0.49",
                    "priceValidUntil": "2050-11-11",
                    "itemCondition": "https://schema.org/UsedCondition",
                    "availability": "https://schema.org/InStock"
                }
            },
            {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "KruiKube",
                "url": "url index",
                "alternateName": "petitedescription",
            }
        ]
    </script>

    <?php include 'footer.php' ?>
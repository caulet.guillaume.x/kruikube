<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="Asset/Image/cubefav.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta property="og:title" content="Kruikube - La décoration innovante">
    <meta property="og:locale" content="fr_FR">

    <meta property="og:description" content="">

    <meta property="og:url" content=" url de notre site ">
    <meta property="og:site_name" content="Kruikube">
    <meta property="og:type" content="website">
    <meta name="keywords" content="Kruikube, Kube, cube, decoration innovante, decoration insolite, decoration zen, ambiance zen" />
    <meta name="robots" content="INDEX, FOLLOW" />

    <meta name="identifier-url" content="url de notre site" />
    <meta name="author" content="Kruikube Simplon Entertainment" />
    <meta name="version" content="2.0" />
    <meta name="copyright" content="Kruikube Corp" />
    <meta http-equiv="Cache-Control" content="no-cache" />


    <link rel="stylesheet" href="style.css">
    <title>Kruikube - La décoration innovante</title>
</head>

<body>
    <?php include 'header.php' ?>
    <h1><a href="index.php">KruiKube</a>, re-découvrez le Kube !</h1>

    <div id="temoignage-container">
        <h2><a href="index.php">KruiKube</a> c'est d'abord un savoir faire artisanal, et ce sont nos employés qui en parlent le mieux...</h2>

        <div class="temoignage">
            <img src="Asset/Image/Romain.jpg" alt="profile de Romain">
            <div class="temoignage-content">
                <h3>Romain - Menuisier non-qualifié chez <a href="index.php">KruiKube</a></h3>
                <p>
                    Ma première experience dans l'aventure <a href="index.php">KruiKube</a>, c'est huit mois en tant que menuisier intérimaire sous-payé.
                    Ils m'ont ensuite donnés la chance de monter en compétences au sein de l'entreprise, actuellement Kube Manager,
                    j'ai la chance de m'occuper d'une floppée de kubers incompétents, et ça, ce n'est pas donné a tout le monde.
                </p>
            </div>
        </div>

        <div class="temoignage">
            <img src="Asset/Image/Hostisse.jpg" alt="profile de Hautisse">
            <div class="temoignage-content">
                <h3>Hostisse - Rédacteur de contenu chez <a href="index.php">KruiKube</a></h3>
                <p>
                    Mais, vous savez, moi je ne crois pas qu’il y ait de bon ou de mauvais <a href="index.php">KruiKube</a>.
                    Si je devais résumer mon expérience chez <a href="index.php">KruiKube</a> Corp
                    je dirais que c’est d’abord des
                    rencontres, des Kruikuber qui m’ont tendu un Kube à un moment
                    où je ne pouvais pas,
                    où j'étais seul dans la startup nation.

                    Et c’est assez amusant de se dire que les Sprint Agile, les meetup
                    peuvent forger un Kube de métal.

                    Parce que quand on a le goût du Kube, quand on a le goût du Kube bien fait,
                    la belle géometrie, parfois on ne trouve pas le Kruikuber en face, je dirais,
                    la kubiformité qui vous permet de kuber.

                    Alors ce n’est pas mon cas, comme je le pitchais,
                    puisque moi au contraire, j’ai pu ; et je dis merci au <a href="index.php">KruiKube</a>, je lui dis merci,
                    je chante le Kruikube, je danse le <a href="index.php">KruiKube</a>… Je ne suis que <a href="index.php">KruiKube</a> !

                    Et finalement, quand beaucoup de gens aujourd’hui me disent :
                    « Mais comment fais-tu pour avoir cette Kruikubité ? »
                    Eh bien je leur réponds très simplement, je leur dis que c’est ce goût du Kruikube,
                    ce goût donc qui m’a poussé aujourd’hui à entreprendre une chirugie Kubique du visage,
                    mais demain, qui sait, peut-être simplement à me mettre au service de la Kruikubauté,
                    à faire le don, le don du... du Kube…
                </p>
            </div>
        </div>

        <div class="temoignage">
            <img src="Asset/Image/Mickael.jpg" alt="profil de Roger">
            <div class="temoignage-content">
                <h3>Mickael - Programmeur en KubeAirNetes chez <a href="index.php">KruiKube</a></h3>
                <p>
                    En tant que programmeur specialiste de la POO (Programmation Orientée Objet),
                    il m'est naturellement apparu un interêt particulier envers l'objet parmis
                    les objets, le Kube.

                    L'abstraction necessaire dont doit faire preuve un esprit brillant comme le mien,
                    192 de QI sur le test partagé sur Facebook par ma mamie, peut être un frein
                    à la bonne redaction du code.
                    Non pour moi bien evidemment, pour mes collegues qui possede
                    un esprit moins brillant intellectuellement, moralement et socialement
                    que le mien, il en est autrement.

                    Cela est d'autant plus le cas lorsque l'on programme en KubeAirNetes
                    sur les fichiers kube.ks vital à chaque projet.

                    Ainsi, posseder un Kruikube, representation physique et materielle d'un Kube
                    de 16.18 cm d’arête qui peut être en bois, en acier Corten ou en plastique bleu,
                    permet aux esprit plus faible que le mien, de le visualiser, et de se rappeler
                    qu'au fond, si le Kube est le plus simple des objets, ne pourrait on pas dire
                    que tous les objets aussi complexe soit leur géometrie ne sont ils pas finalement
                    que des Kubes legerement alterés ?

                    Moi je pense que non, mais j'ai fait croire à mes collegues que oui, et depuis
                    ils en sont persuadés, et ça marche.
                    Nous avons une dizaine de collegues qui sont sorti de leur burnout grace
                    à la nouvelle sect... ou plutot je devrais dire
                    la nouvelle religion du Kube, et qui depuis gambadent dehors dans la garrigue
                    et voit la vie en Kube.

                    Pour tout cela je dit merci Kruikube.


                </p>
            </div>
        </div>
        <div class="temoignage">
            <img src="Asset/Image/Jennifer.jpg" alt="profile de jennifer">
            <div class="temoignage-content">
                <h3>Jennifer - Responsable dirigeante marketing en communication<a href="index.php">KruiKube</a></h3>
                <p>
                    Salut mes petits poussins, moi c'est Jennifer, je suis responsable dirigeante
                    marketting en communication t'as vu, et surtout je suis avant tout
                    sur mon temps libre une influenceuse ASMR sur kwitch, un site
                    de streamkubing en developpement sur lequel je stream en avant première.
                    Ouais car le type qui developpe ce site c'est l'ex de ma pote Alicia mais
                    je l'ai pécho pendant qu'ils sortaient ensemble car je
                </p>
            </div>
        </div>
        <div class="temoignage">
            <img src="Asset/Image/Sam.jpg" alt="profile de Sam">
            <div class="temoignage-content">
                <h3>Interview de Sam concepteur du <a href="index.php">KruiKube</a> (et de ... créateur de l'entreprise)</h3>
                <h4> Samantha (Journaliste): </h4>
                <p> - Bonjour Sam
                    Vous êtes donc l'inventeur du Kruikube. Comment avez vous eu cette idée?
                    Et expliquez nous un peu ce qu'est le Kruikube?
                </p>
                <h4> Sam (inventeur du Kruikube) : </h4>
                <p>
                    Le Kruikube est un objet de décoration qui à la forme d'un cube d'arrête de 16cm.
                    Il existe en bois, acier corten ou plastique bleu.
                    Je dirai que le Kruikube est L'objet de décoration, à la fois l'objet de base et l'ame de la décoration.
                <p> Samantha: - L'Objet, l'Ame de la décoration ? </p>
                <p> Sam: - Je vous explique. Je venez d'aquérir mon tout premier logement,
                    j'avais fini l'aménagement, la partie fonctionnelle, utile, j'en étais à
                    la partie déco.
                    Je cherchais quelque chose d'original, de différents et la nuit,
                    dans un songe me vint la forme idéale: le cube. Toute ces faces sont
                    identiques. C’est sobre, simplifié.
                    La forme idéale mais aussi le symbole idéal : Le cube, c’est une boite,
                    le rangement. Le cube, c'est aussi une pièce du logement ou la maison, le logement entier.
                    Ainsi pour moi le cube pouvait représenter son "chez-soi".
                    Et je suis fabriqué mon premier Kruikube, en bois.
                <p> Samantha: - Alors le Kruikube est la forme et le symbole idéal du rangement, du logement? </p>
                <p> Sam: - Ou plutôt comme la matérialisation de ton nouveau "chez soi", comme une "mascotte", sous
                    la forme ou l'identité de cet objet déco le Kruikube.
                </p>
                <p> Je me suis donc fabriqué mon Kruikube,
                    et j'en ai fabriqué d'autres que j'ai offert à des amis pour leur pendaison de crémaillère.
                    L'idée a plu, ils avaient leur "chez soi" ils avaient donc leur "Kruikube".
                </p>
                <p> Samantha: - Cà a beaucoup plus je crois </p>
                <p> Sam: - Effectivement, tous mes amis voulait avoir leur Kruikube.
                    Et mes amis voulaient aussi offrir un Kruikube à leur amis.
                    Et j'ai du passer d'une production artisanale à plus industrielle.
                    Ainsi nacquit la Entreprise Kruikube
                </p>
                <p> Samantha: - Et je crois que le Kruikube à fait des petits? </p>
                <p> Sam: - Tout à fait, les premiers Kruikube était en bois.
                    Comme je travaillais dans la métallurgie, j'ai voulu en faire en acier Corten.
                    Le Kruikube bois pouvant symboliser "identifier" un logement pavillonnaire ou plus "coocooning".
                    Le Kruikube acier corten pouvant symboliser un logement plus moderne ou dans une grande tour.
                    Et mets ensuite vite apparut l'idée de faire un Kruikube en plastique.
                    je crois que le Kruikube à fait des petits? </p>


                <!-- <p> Sur les autres pages... idées possibles: </p>
                <h4> Témoignage d'un Wood Kruikubeur (utilisateur) de cube déco en bois
                    (sur la page bois)...
                </h4>
                <h4> Témoignage d'un Corten Steel Kruikubeur (utilisateur) de cube déco en acier Corten
                    (sur la page corten)...
                </h4>
                <h4> Témoignage d'un Blue plastic Kruikubeur (utilisateur) de cube déco en plastique bleu
                    (sur la page plastique)...
                </h4> -->
            </div>
        </div>


    </div>

    <?php include 'footer.php' ?>